if (document.querySelectorAll('.chat__inner').length) {
    const chatInnerScrollbar = new PerfectScrollbar('.chat__inner');
}

if (document.querySelectorAll('.list-layout__table-container').length) {
    const tableLayoutScrollbar = new PerfectScrollbar('.list-layout__table-container');
}

if (document.querySelectorAll('.jackpot-layout__list').length) {
    const jackpotLayoutListScrollbar = new PerfectScrollbar('.jackpot-layout__list');
}

if (document.querySelectorAll('.jackpot-layout__main').length) {
    const jackpotLayoutMainScrollbar = new PerfectScrollbar('.jackpot-layout__main');
}

if (document.querySelectorAll('.faq-layout').length) {
    const faqLayoutScrollbar = new PerfectScrollbar('.faq-layout');
}

if (document.querySelectorAll('.terms-layout').length) {
    const termsLayoutScrollbar = new PerfectScrollbar('.terms-layout');
}

if (document.querySelectorAll('.process-layout').length) {
    const processLayoutScrollbar = new PerfectScrollbar('.process-layout');
}

if (document.querySelectorAll('.get-started-layout').length) {
    const getStartedLayoutScrollbar = new PerfectScrollbar('.get-started-layout');
}

if (document.querySelectorAll('.provably-fair-layout').length) {
    const provablyFairLayoutLayoutScrollbar = new PerfectScrollbar('.provably-fair-layout');
}

var modal = new tingle.modal({
    footer: false,
    stickyFooter: false,
    closeMethods: ['overlay', 'escape'],
    closeLabel: "Close",
    cssClass: ['custom-class-1', 'custom-class-2'],
    onOpen: function() {
        document.getElementById('popup-close').addEventListener('click', function (event) {
            modal.close();
        });
    }
});

modal.setContent(' ' +
'<div class="popup">' +
    '<button class="popup__close" id="popup-close">Close</button>' +
    '<div class="popup__avatar">' +
        '<img src="img/user-avatars/155-user1.jpg" alt="" class="popup__avatar-image" width="155">' +
    '</div>' +
    '<h3 class="popup__title">Main with avatar</h3>' +
    '<div class="popup__form">' +
        '<label for="popup__input" class="popup__label">Trade URL:</label>' +
        '<input class="popup__input" type="text" id="popup__input" readonly value="https://steamcommunity.com/tradeoffer/new/?partner=407748149&token=gZ26Sq3f">' +
        '<a href="#" class="button button--small button--green popup__button">Edit profile</a>' +
    '</div>' +
    '<div class="popup__after-form">' +
        '<a href="#" class="popup__find-link">Find your Trade URL here</a>' +
    '</div>' +
'</div>'
);


document.getElementById('toggle-theme').addEventListener('click', function (event) {
    document.body.classList.toggle('theme-white');
});

if (document.getElementById('user-dropdown-button')) {

    document.getElementById('user-dropdown-button').addEventListener('click',function(){
        document.getElementById('user-dropdown').style.display = 'block';
        document.getElementsByClassName('user-dropdown')[0].classList.add('user-dropdown--opened');
    });

    window.onclick = function(event){
        if (event.target.id !== 'user-dropdown-button' && event.target.parentNode.id !== 'user-dropdown-button') {
            document.getElementById('user-dropdown').style.display = 'none';
            document.getElementsByClassName('user-dropdown')[0].classList.remove('user-dropdown--opened');
        }
    }
}


if (document.getElementById('trade-offer-link')) {
    document.getElementById('trade-offer-link').addEventListener('click', function (ev) { modal.open(); ev.preventDefault(); })
}


var coinflipModal =  new tingle.modal({
    footer: false,
    stickyFooter: false,
    closeMethods: ['overlay', 'escape'],
    closeLabel: "Close",
    cssClass: ['history-modal'],
    onOpen: function() {
        document.getElementById('popup-close').addEventListener('click', function (event) {
            coinflipModal.close();
        });

        if (document.getElementById('history-coinflip-button')) {

            document.getElementById('history-coinflip-button').addEventListener('click',function(){
                document.getElementById('history-coinflip-list').style.display = 'block';
                document.getElementById('history-coinflip-button').classList.add('history-button--opened');
            });

            window.onclick = function(event){
                if (event.target.id !== 'history-coinflip-button') {
                    document.getElementById('history-coinflip-list').style.display = 'none';
                    document.getElementById('history-coinflip-button').classList.remove('history-button--opened');
                }
            }
        }
    }
});

coinflipModal.setContent(' ' +
'<div class="popup">' +
    '<button class="popup__close" id="popup-close">Close</button>' +
    '<div class="history-head">' +
        '<h3 class="popup__title"><span class="history-head__coinflip-icon"></span>История коинфлипа</h3>' +
        '<div class="history-dropdown-wrapper">' +
            '<button id="history-coinflip-button" class="history-button" type="button">Прошлый месяц</button>' +
            '<ul id="history-coinflip-list" class="history-dropdown-list">' +
                '<li class="history-dropdown-list__item"><a class="history-dropdown-list__link" href="#">3 месяца назад</a></li>' +
                '<li class="history-dropdown-list__item"><a class="history-dropdown-list__link" href="#">Полгода назад</a></li>' +
                '<li class="history-dropdown-list__item"><a class="history-dropdown-list__link" href="#">Прошлая неделя</a></li>' +
            '</ul>' +
        '</div>' +
    '</div>' +
    '<table class="history-list">' +
        '<tr>' +
            '<th>Игроки</th>' +
            '<th>Предметы</th>' +
            '<th>Всего</th>' +
        '</tr>' +
        '<tr>' +
            '<td><span class="history-list__strong">24</span> предмета</td>' +
            '<td>2000</td>' +
            '<td>23</td>' +
        '</tr>' +
        '<tr>' +
            '<td><span class="history-list__strong">24</span> предметов</td>' +
            '<td>200</td>' +
            '<td>10</td>' +
        '</tr>' +
        '<tr>' +
            '<td><span class="history-list__strong">24</span> предметов</td>' +
            '<td>200</td>' +
            '<td>10</td>' +
        '</tr>' +
        '<tr>' +
            '<td><span class="history-list__strong">24</span> предметов</td>' +
            '<td>200</td>' +
            '<td>10</td>' +
        '</tr>' +
        '<tr>' +
            '<td><span class="history-list__strong">24</span> предметов</td>' +
            '<td>200</td>' +
            '<td>10</td>' +
        '</tr>' +
    '</table>' +
    '<div class="history-footer"><button type="button" class="button button--large button--green">OK</button></div>' +
'</div>'
);

var jackpotModal =  new tingle.modal({
    footer: false,
    stickyFooter: false,
    closeMethods: ['overlay', 'escape'],
    closeLabel: "Close",
    cssClass: ['history-modal'],
    onOpen: function() {
        document.getElementById('popup-close').addEventListener('click', function (event) {
            jackpotModal.close();
        });

        if (document.getElementById('history-jackpot-button')) {

            document.getElementById('history-jackpot-button').addEventListener('click',function(){
                document.getElementById('history-jackpot-list').style.display = 'block';
                document.getElementById('history-jackpot-button').classList.add('history-button--opened');
            });

            window.onclick = function(event){
                if (event.target.id !== 'history-jackpot-button') {
                    document.getElementById('history-jackpot-list').style.display = 'none';
                    document.getElementById('history-jackpot-button').classList.remove('history-button--opened');
                }
            }
        }
    }
});

jackpotModal.setContent(' ' +
    '<div class="popup">' +
        '<button class="popup__close" id="popup-close">Close</button>' +
        '<div class="history-head">' +
            '<h3 class="popup__title"><span class="history-head__jackpot-icon"></span>История игр Джекпота</h3>' +
            '<div class="history-dropdown-wrapper">' +
                '<button id="history-jackpot-button" class="history-button" type="button">Прошлый месяц</button>' +
                '<ul id="history-jackpot-list" class="history-dropdown-list">' +
                    '<li class="history-dropdown-list__item"><a class="history-dropdown-list__link" href="#">3 месяца назад</a></li>' +
                    '<li class="history-dropdown-list__item"><a class="history-dropdown-list__link" href="#">Полгода назад</a></li>' +
                    '<li class="history-dropdown-list__item"><a class="history-dropdown-list__link" href="#">Прошлая неделя</a></li>' +
                '</ul>' +
            '</div>' +
        '</div>' +
        '<div class="history-stats">' +
            '<div class="history-stats__item">' +
                '<div class="history-stats__title">Общая ставка</div>' +
                '<div class="history-stats__total"><span class="rouble rouble--bold">й</span> 12000.10</div>' +
            '</div>' +
            '<div class="history-stats__item">' +
                '<div class="history-stats__title">Всего выиграли</div>' +
                '<div class="history-stats__total"><span class="rouble rouble--bold">й</span> 12000.10</div>' +
            '</div>' +
            '<div class="history-stats__item">' +
                '<div class="history-stats__title">Прибыль</div>' +
                '<div class="history-stats__total"><span class="rouble rouble--bold">й</span> 12000.10</div>' +
            '</div>' +
        '</div>' +
        '<table class="history-list">' +
            '<tr>' +
                '<th>Предметов</th>' +
                '<th>Итоговая сумма</th>' +
                '<th>Выиграли</th>' +
                '<th>Проиграли</th>' +
                '<th>Время</th>' +
            '</tr>' +
            '<tr>' +
                '<td><span class="history-list__strong">24</span> предмета</td>' +
                '<td><span class="rouble rouble--bold">й</span> 6000.49</td>' +
                '<td>1008</td>' +
                '<td>245</td>' +
                '<td>23</td>' +
            '</tr>' +
        '</table>' +
        '<div class="history-footer"><button type="button" class="button button--large button--green">OK</button></div>' +
    '</div>'
);

if (document.getElementById('coinflip-history-link')) {
    document.getElementById('coinflip-history-link').addEventListener('click', function (ev) { coinflipModal.open(); ev.preventDefault(); })
}

if (document.getElementById('jackpot-history-link')) {
    document.getElementById('jackpot-history-link').addEventListener('click', function (ev) { jackpotModal.open(); ev.preventDefault(); })
}








var createCoinflipModal =  new tingle.modal({
    footer: false,
    stickyFooter: false,
    closeMethods: ['overlay', 'escape'],
    closeLabel: "Close",
    cssClass: ['create-coinflip-modal'],
    onOpen: function() {
        document.getElementById('popup-close').addEventListener('click', function (event) {
            createCoinflipModal.close();
        });
    }
});

createCoinflipModal.setContent(' ' +
    '<div class="popup">' +
        '<button class="popup__close" id="popup-close">Close</button>' +
        '<div class="history-head">' +
            '<h3 class="popup__title"><span class="history-head__coinflip-icon"></span>Create new Coinflip</h3>' +
        '</div>' +
        '<div class="create-coinflip-head">' +
            '<div class="create-coinflip-head__left">' +
                '<div class="create-coinflip-head__min"><span class="create-coinflip-head__label">Min.</span>$ <strong>8</strong></div>' +
                '<div class="create-coinflip-head__max"><span class="create-coinflip-head__label">Max.</span><strong>12</strong> items</div>' +
                '<div class="create-coinflip-head__minmax-description">Select from your items,choose team if needs, and then click to Send Trade Request. You will receive an offer contains the selected items witch you can confirm</div>' +
            '</div>' +
        '</div>' +
        '<div class="create-coinflip-inventory">' +
            '<h4 class="create-coinflip-inventory__title">Your inventory</h4>' +
            '<div class="create-coinflip-inventory__action">' +
                '<button class="create-coinflip-inventory__action-btn" type="button"><span class="refresh-inventory"></span>Refresh inventory</button>' +
            '</div>' +
            '<div class="create-coinflip-inventory__inventory-list">' +
                '<div class="game-items-tiles">' +
                    '<div class="game-items-tile">' +
                        '<div class="game-items-tile__label game-items-tile__label--left">FN</div>' +
                        '<img src="img/game-item-large-1.png" alt="" class="game-items-tile__image" width="71" height="35">' +
                        '<div class="game-items-tile__description">' +
                            '<div class="game-items-tile__price">$ 12’49</div>' +
                            '<div class="game-items-tile__info">' +
                                '<span class="game-items-tile__info-item">AUG</span>' +
                                '<span class="game-items-tile__info-item">Bengal Tiger</span>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                    '<div class="game-items-tile">' +
                        '<div class="game-items-tile__label game-items-tile__label--left">FN</div>' +
                        '<div class="game-items-tile__label game-items-tile__label--right">ST</div>' +
                        '<img src="img/game-item-large-1.png" alt="" class="game-items-tile__image" width="71" height="35">' +
                        '<div class="game-items-tile__description">' +
                            '<div class="game-items-tile__price">$ 12’49</div>' +
                            '<div class="game-items-tile__info">' +
                                '<span class="game-items-tile__info-item">AUG</span>' +
                                '<span class="game-items-tile__info-item">Bengal Tiger</span>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                    '<div class="game-items-tile">' +
                        '<div class="game-items-tile__label game-items-tile__label--left">FN</div>' +
                        '<div class="game-items-tile__label game-items-tile__label--right">ST</div>' +
                        '<img src="img/game-item-large-1.png" alt="" class="game-items-tile__image" width="71" height="35">' +
                        '<div class="game-items-tile__description">' +
                            '<div class="game-items-tile__price">$ 12’49</div>' +
                            '<div class="game-items-tile__info">' +
                                '<span class="game-items-tile__info-item">AUG</span>' +
                                '<span class="game-items-tile__info-item">Bengal Tiger</span>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>' +
        '</div>' +
        '<div class="create-coinflip-footer"><button type="button" class="button button--outline button--green create-coinflip-footer__button">Cancel</button></div>' +
    '</div>'
);


var createCoinflipModalOption2 = new tingle.modal({
    footer: false,
    stickyFooter: false,
    closeMethods: ['overlay', 'escape'],
    closeLabel: "Close",
    cssClass: ['create-coinflip-modal'],
    onOpen: function() {
        document.getElementById('popup-close').addEventListener('click', function (event) {
            createCoinflipModalOption2.close();
        });
    }
});

createCoinflipModalOption2.setContent(' ' +
    '<div class="popup">' +
        '<button class="popup__close" id="popup-close">Close</button>' +
        '<div class="history-head">' +
            '<h3 class="popup__title"><span class="history-head__coinflip-icon"></span>Create new Coinflip</h3>' +
        '</div>' +
        '<div class="create-coinflip-head">' +
            '<div class="create-coinflip-head__left">' +
                '<div class="create-coinflip-head__min"><span class="create-coinflip-head__label">Min.</span>$ <strong>8</strong></div>' +
                '<div class="create-coinflip-head__max"><span class="create-coinflip-head__label">Max.</span><strong>12</strong> items</div>' +
                '<div class="create-coinflip-head__minmax-description">Select from your items,choose team if needs, and then click to Send Trade Request. You will receive an offer contains the selected items witch you can confirm</div>' +
            '</div>' +
        '</div>' +
        '<div class="create-coinflip-inventory">' +
            '<h4 class="create-coinflip-inventory__title">Your inventory</h4>' +
            '<div class="create-coinflip-inventory__action">' +
                '<button class="create-coinflip-inventory__action-btn" type="button"><span class="refresh-inventory"></span>Could refresh in 6 secs</button>' +
            '</div>' +
            '<div class="create-coinflip-inventory__inventory-list">' +
                '<div class="game-items-tiles">' +
                    '<div class="game-items-tile">' +
                        '<div class="game-items-tile__label game-items-tile__label--left">FN</div>' +
                        '<img src="img/game-item-large-1.png" alt="" class="game-items-tile__image" width="71" height="35">' +
                        '<div class="game-items-tile__description">' +
                            '<div class="game-items-tile__price">$ 12’49</div>' +
                            '<div class="game-items-tile__info">' +
                                '<span class="game-items-tile__info-item">AUG</span>' +
                                '<span class="game-items-tile__info-item">Bengal Tiger</span>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                    '<div class="game-items-tile">' +
                        '<div class="game-items-tile__label game-items-tile__label--left">FN</div>' +
                        '<div class="game-items-tile__label game-items-tile__label--right">ST</div>' +
                        '<img src="img/game-item-large-1.png" alt="" class="game-items-tile__image" width="71" height="35">' +
                        '<div class="game-items-tile__description">' +
                            '<div class="game-items-tile__price">$ 12’49</div>' +
                            '<div class="game-items-tile__info">' +
                                '<span class="game-items-tile__info-item">AUG</span>' +
                                '<span class="game-items-tile__info-item">Bengal Tiger</span>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                    '<div class="game-items-tile">' +
                        '<div class="game-items-tile__label game-items-tile__label--left">FN</div>' +
                        '<div class="game-items-tile__label game-items-tile__label--right">ST</div>' +
                        '<img src="img/game-item-large-1.png" alt="" class="game-items-tile__image" width="71" height="35">' +
                        '<div class="game-items-tile__description">' +
                            '<div class="game-items-tile__price">$ 12’49</div>' +
                            '<div class="game-items-tile__info">' +
                                '<span class="game-items-tile__info-item">AUG</span>' +
                                '<span class="game-items-tile__info-item">Bengal Tiger</span>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>' +
        '</div>' +
        '<div class="create-coinflip-footer"><button type="button" class="button button--outline button--green create-coinflip-footer__button">Cancel</button></div>' +
    '</div>'
);

var createCoinflipModalOption3 = new tingle.modal({
    footer: false,
    stickyFooter: false,
    closeMethods: ['overlay', 'escape'],
    closeLabel: "Close",
    cssClass: ['create-coinflip-modal'],
    onOpen: function() {
        document.getElementById('popup-close').addEventListener('click', function (event) {
            createCoinflipModalOption3.close();
        });
    }
});

createCoinflipModalOption3.setContent(' ' +
    '<div class="popup">' +
        '<button class="popup__close" id="popup-close">Close</button>' +
        '<div class="history-head">' +
            '<h3 class="popup__title"><span class="history-head__coinflip-icon"></span>Create new Coinflip</h3>' +
        '</div>' +
        '<div class="create-coinflip-head">' +
            '<div class="create-coinflip-head__left">' +
                '<div class="create-coinflip-head__min"><span class="create-coinflip-head__label">Min.</span>$ <strong>8</strong></div>' +
                '<div class="create-coinflip-head__max"><span class="create-coinflip-head__label">Max.</span><strong>12</strong> items</div>' +
                '<div class="create-coinflip-head__minmax-description">Select from your items,choose team if needs, and then click to Send Trade Request. You will receive an offer contains the selected items witch you can confirm</div>' +
            '</div>' +
            '<div class="create-coinflip-head__right">' +
                '<div class="create-coinflip-head__coins">' +
                    '<span class="create-coinflip-head__coin create-coinflip-head__coin--heads create-coinflip-head__coin--active"></span>' +
                    '<span class="create-coinflip-head__coin create-coinflip-head__coin--tails"></span>' +
                '</div>' +
                '<div class="create-coinflip-head__total">' +
                    '<span class="create-coinflip-head__label">Total</span> <strong>4</strong> items' +
                '</div>' +
                '<div class="create-coinflip-head__valued-at">' +
                    '<span class="create-coinflip-head__label">Valued at</span>$ <strong>45</strong>' +
                '</div>' +
                '<div class="create-coinflip-head__deposit">' +
                    '<button type="button" class="button button--green button--small create-coinflip-head__deposit-btn">Deposit</button>' +
                '</div>' +
            '</div>' +
        '</div>' +
        '<div class="create-coinflip-inventory">' +
            '<h4 class="create-coinflip-inventory__title">Your inventory</h4>' +
            '<div class="create-coinflip-inventory__action">' +
                '<button class="create-coinflip-inventory__action-btn" type="button"><span class="refresh-inventory"></span>Could refresh in 6 secs</button>' +
            '</div>' +
            '<div class="create-coinflip-inventory__inventory-list">' +
                '<div class="game-items-tiles">' +
                    '<div class="game-items-tile create-coinflip-inventory__highlighted-tile">' +
                        '<div class="game-items-tile__label game-items-tile__label--left">FN</div>' +
                        '<img src="img/game-item-large-1.png" alt="" class="game-items-tile__image" width="71" height="35">' +
                        '<div class="game-items-tile__description">' +
                            '<div class="game-items-tile__price">$ 12’49</div>' +
                            '<div class="game-items-tile__info">' +
                                '<span class="game-items-tile__info-item">AUG</span>' +
                                '<span class="game-items-tile__info-item">Bengal Tiger</span>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                    '<div class="game-items-tile create-coinflip-inventory__highlighted-tile">' +
                        '<div class="game-items-tile__label game-items-tile__label--left">FN</div>' +
                        '<div class="game-items-tile__label game-items-tile__label--right">ST</div>' +
                        '<img src="img/game-item-large-1.png" alt="" class="game-items-tile__image" width="71" height="35">' +
                        '<div class="game-items-tile__description">' +
                            '<div class="game-items-tile__price">$ 12’49</div>' +
                            '<div class="game-items-tile__info">' +
                                '<span class="game-items-tile__info-item">AUG</span>' +
                                '<span class="game-items-tile__info-item">Bengal Tiger</span>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                    '<div class="game-items-tile create-coinflip-inventory__highlighted-tile">' +
                        '<div class="game-items-tile__label game-items-tile__label--left">FN</div>' +
                        '<div class="game-items-tile__label game-items-tile__label--right">ST</div>' +
                        '<img src="img/game-item-large-1.png" alt="" class="game-items-tile__image" width="71" height="35">' +
                        '<div class="game-items-tile__description">' +
                            '<div class="game-items-tile__price">$ 12’49</div>' +
                            '<div class="game-items-tile__info">' +
                                '<span class="game-items-tile__info-item">AUG</span>' +
                                '<span class="game-items-tile__info-item">Bengal Tiger</span>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>' +
        '</div>' +
        '<div class="create-coinflip-footer"><button type="button" class="button button--outline button--green create-coinflip-footer__button">Cancel</button></div>' +
    '</div>'
);



var createCoinflipModalOption4 = new tingle.modal({
    footer: false,
    stickyFooter: false,
    closeMethods: ['overlay', 'escape'],
    closeLabel: "Close",
    cssClass: ['create-coinflip-modal'],
    onOpen: function() {
        document.getElementById('popup-close').addEventListener('click', function (event) {
            createCoinflipModalOption4.close();
        });
    }
});

createCoinflipModalOption4.setContent(' ' +
    '<div class="popup">' +
        '<button class="popup__close" id="popup-close">Close</button>' +
        '<div class="history-head">' +
            '<h3 class="popup__title"><span class="history-head__coinflip-icon"></span>Create new Coinflip</h3>' +
        '</div>' +
        '<div class="create-coinflip-head">' +
            '<div class="create-coinflip-head__left">' +
                '<div class="create-coinflip-head__min"><span class="create-coinflip-head__label">Min.</span>$ <strong>8</strong></div>' +
                '<div class="create-coinflip-head__max"><span class="create-coinflip-head__label">Max.</span><strong>12</strong> items</div>' +
                '<div class="create-coinflip-head__minmax-description">Select from your items,choose team if needs, and then click to Send Trade Request. You will receive an offer contains the selected items witch you can confirm</div>' +
            '</div>' +
        '</div>' +
        '<div class="create-coinflip-inventory">' +
            '<h4 class="create-coinflip-inventory__title">Your inventory</h4>' +
            '<div class="create-coinflip-inventory__inventory-list">' +
                '<h4 class="create-coinflip-inventory__sending-title">We are sending your offer</h4>' +
                '<div class="sending-offer"></div>' +
                '<p class="create-coinflip-inventory__sending-paragraph">You will receive your offers link here.<br>Always use the links we provide to you or chek the security code of trade to defense against fake bots or scammers</p>' +
            '</div>' +
        '</div>' +
        '<div class="create-coinflip-footer"><button type="button" class="button button--outline button--green create-coinflip-footer__button">Cancel</button></div>' +
    '</div>'
);



var createCoinflipModalOption5 = new tingle.modal({
    footer: false,
    stickyFooter: false,
    closeMethods: ['overlay', 'escape'],
    closeLabel: "Close",
    cssClass: ['create-coinflip-modal'],
    onOpen: function() {
        document.getElementById('popup-close').addEventListener('click', function (event) {
            createCoinflipModalOption5.close();
        });
    }
});

createCoinflipModalOption5.setContent(' ' +
    '<div class="popup">' +
        '<button class="popup__close" id="popup-close">Close</button>' +
        '<div class="history-head">' +
            '<h3 class="popup__title"><span class="history-head__coinflip-icon"></span>Create new Coinflip</h3>' +
        '</div>' +
        '<div class="create-coinflip-head">' +
            '<div class="create-coinflip-head__left">' +
            '<div class="create-coinflip-head__min"><span class="create-coinflip-head__label">Min.</span>$ <strong>8</strong></div>' +
            '<div class="create-coinflip-head__max"><span class="create-coinflip-head__label">Max.</span><strong>12</strong> items</div>' +
            '<div class="create-coinflip-head__minmax-description">Select from your items,choose team if needs, and then click to Send Trade Request. You will receive an offer contains the selected items witch you can confirm</div>' +
            '</div>' +
        '</div>' +
        '<div class="create-coinflip-inventory">' +
            '<h4 class="create-coinflip-inventory__title">Your inventory</h4>' +
            '<div class="create-coinflip-inventory__inventory-list">' +
                '<h4 class="create-coinflip-inventory__active-title">Offer is active</h4>' +
                '<p class="create-coinflip-inventory__active-paragraph">Reference: 5assdfmmviir4578gkgmb, Security code: Tajjdk</p>' +
                '<div class="create-coinflip-inventory__sending-buttons"><a href="#" class="button button--green create-coinflip-inventory__sending-btn"><span class="sending-offer-browser-icon"></span>Open in browser</a> <button type="button" class="button button--green create-coinflip-inventory__sending-btn"><span class="sending-offer-steam-icon"></span>Open in Steam</button></div>' +
                '<p class="create-coinflip-inventory__active-paragraph">Always use the links we provide to you or chek the security code of trade to defense against fake bots or scammers</p>' +
            '</div>' +
        '</div>' +
        '<div class="create-coinflip-footer"><button type="button" class="button button--outline button--green create-coinflip-footer__button">Cancel</button></div>' +
    '</div>'
);


var coinflipHistoryModal = new tingle.modal({
    footer: false,
    stickyFooter: false,
    closeMethods: ['overlay', 'escape'],
    closeLabel: "Close",
    cssClass: ['coinflip-history-modal'],
    onOpen: function() {
        document.getElementById('popup-close').addEventListener('click', function (event) {
            coinflipHistoryModal.close();
        });
    }
});

coinflipHistoryModal.setContent(' ' +
    '<div class="popup">' +
        '<button class="popup__close" id="popup-close">Close</button>' +
        '<div class="history-head">' +
            '<h3 class="popup__title"><span class="history-head__coinflip-icon"></span>Coinflip history <small>( last month )</small></h3>' +
        '</div>' +
        '<table class="coinflip-history__table-head">' +
            '<tr>' +
                '<th class="coinflip-history__players-col">Players</th>' +
                '<th class="coinflip-history__items-col">Items</th>' +
                '<th class="coinflip-history__total-col">Total</th>' +
                '<th class="coinflip-history__winner"></th>' +
            '</tr>' +
        '</table>' +
        '<div class="coinflip-history__table-container">' +
            '<table class="coinflip-history__table">' +
                '<tr>' +
                    '<td class="coinflip-history__players-col">' +
                        '<img src="img/user-avatars/32-user1.jpg" alt="" class="coinflip-history__avatar" width="32" height="32">' +
                        'vs' +
                        '<img src="img/user-avatars/32-user2.jpg" alt="" class="coinflip-history__avatar" width="32" height="32">' +
                    '</td>' +
                    '<td class="coinflip-history__items-col">' +
                        '<ul class="coinflip-history__game-items">' +
                            '<li class="coinflip-history__game-item">' +
                                '<img src="img/game-item-1.png" alt="" class="coinflip-history__game-item-image" width="38" height="26">' +
                            '</li>' +
                            '<li class="coinflip-history__game-item">' +
                                '<img src="img/game-item-2.png" alt="" class="coinflip-history__game-item-image" width="39" height="29">' +
                            '</li>' +
                            '<li class="coinflip-history__game-item">' +
                                '<img src="img/game-item-1.png" alt="" class="coinflip-history__game-item-image" width="38" height="26">' +
                            '</li>' +
                            '<li class="coinflip-history__game-item">' +
                                '<img src="img/game-item-2.png" alt="" class="coinflip-history__game-item-image" width="39" height="29">' +
                            '</li>' +
                            '<li class="coinflip-history__game-item">' +
                                '<img src="img/game-item-2.png" alt="" class="coinflip-history__game-item-image" width="39" height="29">' +
                            '</li>' +
                            '<li class="coinflip-history__game-item">' +
                                '<img src="img/game-item-1.png" alt="" class="coinflip-history__game-item-image" width="38" height="26">' +
                            '</li>' +
                            '<li class="coinflip-history__game-item">' +
                                '<img src="img/game-item-2.png" alt="" class="coinflip-history__game-item-image" width="39" height="29">' +
                            '</li>' +
                            '<li class="coinflip-history__game-item">' +
                                '<img src="img/game-item-1.png" alt="" class="coinflip-history__game-item-image" width="38" height="26">' +
                            '</li>' +
                            '<li class="coinflip-history__game-item">' +
                                '<img src="img/game-item-2.png" alt="" class="coinflip-history__game-item-image" width="39" height="29">' +
                            '</li>' +
                            '<li class="coinflip-history__game-item">' +
                                '<span class="coinflip-history__game-label">12 items</span>' +
                            '</li>' +
                        '</ul>' +
                    '</td>' +
                    '<td class="coinflip-history__total-col">$ <strong>8</strong></td>' +
                    '<td class="coinflip-history__winner-col">' +
                        '<div class="coinflip-history__coin coinflip-history__coin--heads"></div>' +
                        '<img src="img/user-avatars/32-user4.jpg" alt="" class="coinflip-history__avatar" width="32" height="32">' +
                    '</td>' +
                '</tr>' +
            '</table>' +
        '</div>' +
        '<div class="coinflip-history__table-comment"><span>Hesh: 122333999900fvdvbjalgb7878</span><span>Percentage: 45.344455 %</span><span>Secret: Wkaart</span><button class="coinflip-history__help-icon"></button></div>' +
        '<div class="create-coinflip-footer">' +
            '<div class="modal-footer-pagination">' +
                '<a href="#" class="modal-footer-pagination__item">1</a>' +
                '<a href="#" class="modal-footer-pagination__item">2</a>' +
                '<a href="#" class="modal-footer-pagination__item">3</a>' +
                '<a href="#" class="modal-footer-pagination__item">4</a>' +
                '<a href="#" class="modal-footer-pagination__item">5</a>' +
                '...' +
                '<a href="#" class="modal-footer-pagination__item">10</a>' +
            '</div>' +
            '<button type="button" class="button button--outline button--green create-coinflip-footer__button">OK</button>' +
        '</div>' +
    '</div>'
);

var jackpotHistoryModal = new tingle.modal({
    footer: false,
    stickyFooter: false,
    closeMethods: ['overlay', 'escape'],
    closeLabel: "Close",
    cssClass: ['jackpot-history-modal'],
    onOpen: function() {
        document.getElementById('popup-close').addEventListener('click', function (event) {
            jackpotHistoryModal.close();
        });
    }
});

jackpotHistoryModal.setContent(' ' +
    '<div class="popup">' +
    '<button class="popup__close" id="popup-close">Close</button>' +
    '<div class="history-head">' +
        '<h3 class="popup__title"><span class="history-head__jackpot-icon"></span>Jackpot history <small>( last month )</small></h3>' +
    '</div>' +
    '<div class="jackpot-history-overview">' +
        '<div class="jackpot-history-overview__item">' +
            '<span class="jackpot-history-overview__label">Total bet</span>' +
            '$ 8.109' +
        '</div>' +
        '<div class="jackpot-history-overview__item">' +
            '<span class="jackpot-history-overview__label">Total won</span>' +
            '$ 8.109' +
        '</div>' +
        '<div class="jackpot-history-overview__item">' +
            '<span class="jackpot-history-overview__label">Profit</span>' +
            '$ 8.109' +
        '</div>' +
    '</div>' +
    '<table class="jackpot-history__table-head">' +
        '<tr>' +
            '<th class="jackpot-history__items-col">Items</th>' +
            '<th class="jackpot-history__amount-col">Amount</th>' +
            '<th class="jackpot-history__winlose-col">Win/lose</th>' +
            '<th class="jackpot-history__time">Time</th>' +
        '</tr>' +
    '</table>' +
    '<div class="jackpot-history__table-container">' +
        '<table class="jackpot-history__table">' +
            '<tr>' +
                '<td class="jackpot-history__items-col">' +
                    '<ul class="jackpot-history__game-items">' +
                        '<li class="jackpot-history__game-item">' +
                            '<img src="img/game-item-1.png" alt="" class="coinflip-history__game-item-image" width="38" height="26">' +
                        '</li>' +
                        '<li class="jackpot-history__game-item">' +
                            '<img src="img/game-item-2.png" alt="" class="coinflip-history__game-item-image" width="39" height="29">' +
                        '</li>' +
                        '<li class="jackpot-history__game-item">' +
                            '<img src="img/game-item-1.png" alt="" class="coinflip-history__game-item-image" width="38" height="26">' +
                        '</li>' +
                        '<li class="jackpot-history__game-item">' +
                            '<img src="img/game-item-2.png" alt="" class="coinflip-history__game-item-image" width="39" height="29">' +
                        '</li>' +
                        '<li class="jackpot-history__game-item">' +
                            '<img src="img/game-item-2.png" alt="" class="coinflip-history__game-item-image" width="39" height="29">' +
                        '</li>' +
                        '<li class="jackpot-history__game-item">' +
                            '<img src="img/game-item-1.png" alt="" class="coinflip-history__game-item-image" width="38" height="26">' +
                        '</li>' +
                        '<li class="jackpot-history__game-item">' +
                            '<img src="img/game-item-2.png" alt="" class="coinflip-history__game-item-image" width="39" height="29">' +
                        '</li>' +
                        '<li class="jackpot-history__game-item">' +
                            '<img src="img/game-item-1.png" alt="" class="coinflip-history__game-item-image" width="38" height="26">' +
                        '</li>' +
                        '<li class="jackpot-history__game-item">' +
                            '<img src="img/game-item-2.png" alt="" class="coinflip-history__game-item-image" width="39" height="29">' +
                        '</li>' +
                        '<li class="jackpot-history__game-item">' +
                            '<img src="img/game-item-2.png" alt="" class="coinflip-history__game-item-image" width="39" height="29">' +
                        '</li>' +
                        '<li class="jackpot-history__game-item">' +
                            '<img src="img/game-item-1.png" alt="" class="coinflip-history__game-item-image" width="39" height="29">' +
                        '</li>' +
                        '<li class="jackpot-history__game-item">' +
                            '<img src="img/game-item-2.png" alt="" class="coinflip-history__game-item-image" width="39" height="29">' +
                        '</li>' +
                        '<li class="jackpot-history__game-item">' +
                            '<span class="jackpot-history__game-label">+12 more</span>' +
                        '</li>' +
                    '</ul>' +
                '</td>' +
                '<td class="jackpot-history__amount-col">' +
                    '$ 8.109' +
                '</td>' +
                '<td class="jackpot-history__winlose-col"><span class="jackpot-history__winlose-icon jackpot-history__winlose-icon--win"></span></td>' +
                '<td class="jackpot-history__time">' +
                    '<div class="jackpot-history__calendar"><div class="jackpot-history__calendar-tooltip">07/01/2018 03:38 a.m. CET</div></div>' +
                '</td>' +
            '</tr>' +
        '</table>' +
    '</div>' +
    '<div class="coinflip-history__table-comment"><span>Hesh: 122333999900fvdvbjalgb7878</span><span>Percentage: 45.344455 %</span><span>Secret: Wkaart</span><button class="coinflip-history__help-icon"></button></div>' +
    '<div class="jackpot-history__table-container">' +
    '<table class="jackpot-history__table">' +
    '<tr>' +
    '<td class="jackpot-history__items-col">' +
    '<ul class="jackpot-history__game-items">' +
    '<li class="jackpot-history__game-item">' +
    '<img src="img/game-item-1.png" alt="" class="jackpot-history__game-item-image" width="38" height="26">' +
    '</li>' +
    '<li class="jackpot-history__game-item">' +
    '<img src="img/game-item-2.png" alt="" class="jackpot-history__game-item-image" width="39" height="29">' +
    '</li>' +
    '<li class="jackpot-history__game-item">' +
    '<img src="img/game-item-1.png" alt="" class="jackpot-history__game-item-image" width="38" height="26">' +
    '</li>' +
    '<li class="jackpot-history__game-item">' +
    '<img src="img/game-item-2.png" alt="" class="jackpot-history__game-item-image" width="39" height="29">' +
    '</li>' +
    '<li class="jackpot-history__game-item">' +
    '<img src="img/game-item-2.png" alt="" class="jackpot-history__game-item-image" width="39" height="29">' +
    '</li>' +
    '<li class="jackpot-history__game-item">' +
    '<img src="img/game-item-1.png" alt="" class="jackpot-history__game-item-image" width="38" height="26">' +
    '</li>' +
    '<li class="jackpot-history__game-item">' +
    '<img src="img/game-item-2.png" alt="" class="jackpot-history__game-item-image" width="39" height="29">' +
    '</li>' +
    '<li class="jackpot-history__game-item">' +
    '<img src="img/game-item-1.png" alt="" class="jackpot-history__game-item-image" width="38" height="26">' +
    '</li>' +
    '<li class="jackpot-history__game-item">' +
    '<img src="img/game-item-2.png" alt="" class="jackpot-history__game-item-image" width="39" height="29">' +
    '</li>' +
    '<li class="jackpot-history__game-item">' +
    '<img src="img/game-item-1.png" alt="" class="jackpot-history__game-item-image" width="39" height="29">' +
    '</li>' +
    '<li class="jackpot-history__game-item">' +
    '<img src="img/game-item-2.png" alt="" class="jackpot-history__game-item-image" width="39" height="29">' +
    '</li>' +
    '<li class="jackpot-history__game-item">' +
    '<img src="img/game-item-1.png" alt="" class="jackpot-history__game-item-image" width="39" height="29">' +
    '</li>' +
    '<li class="jackpot-history__game-item">' +
    '<span class="jackpot-history__game-label">+99 more</span>' +
    '</li>' +
    '</ul>' +
    '</td>' +
    '<td class="jackpot-history__amount-col">' +
    '$ 8.109' +
    '</td>' +
    '<td class="jackpot-history__winlose-col"><span class="jackpot-history__winlose-icon jackpot-history__winlose-icon--lose"></span></td>' +
    '<td class="jackpot-history__time">' +
    '<div class="jackpot-history__calendar"><div class="jackpot-history__calendar-tooltip">07/01/2018 03:38 a.m. CET</div></div>' +
    '</td>' +
    '</tr>' +
    '</table>' +
    '</div>' +
    '<div class="coinflip-history__table-comment"><span>Hesh: 122333999900fvdvbjalgb7878</span><span>Percentage: 45.344455 %</span><span>Secret: Wkaart</span><button class="coinflip-history__help-icon"></button></div>' +
    '<div class="create-coinflip-footer">' +
        '<div class="modal-footer-pagination">' +
            '<a href="#" class="modal-footer-pagination__item">1</a>' +
            '<a href="#" class="modal-footer-pagination__item">2</a>' +
            '<a href="#" class="modal-footer-pagination__item">3</a>' +
            '<a href="#" class="modal-footer-pagination__item">4</a>' +
            '<a href="#" class="modal-footer-pagination__item">5</a>' +
            '...' +
            '<a href="#" class="modal-footer-pagination__item">10</a>' +
        '</div>' +
    '<button type="button" class="button button--outline button--green create-coinflip-footer__button">OK</button></div>' +
    '</div>'
);