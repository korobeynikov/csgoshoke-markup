var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var path = require('path');
var cssBase64 = require('gulp-css-base64');

gulp.task('sass', function () {
    return gulp.src('./sass/**/*.scss')
        .pipe(sass({
            paths: [ path.join(__dirname, 'sass', 'includes') ]
        }))
        .pipe(autoprefixer({
            browsers: ['>0.05%'],
            cascade: false
        }))
        .pipe(cssBase64({
            extensionsAllowed: ['.svg', '.png']
            // extensionsAllowed: ['.png']
        }))
        .pipe(gulp.dest('./css'));
});

gulp.task('optimize-svg', function () {
    return gulp.src('./img/**/*.svg')
        .pipe()
});

gulp.task('default', ['sass']);